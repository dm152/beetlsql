BeetlSQL 集成与其他框架的核心代码,包括
* Spring集成
* Spring Boot集成,附带多数据源，Swagger例子
* Act集成
* Solon集成
* Jfinal集成


单元测试集成使用了H2数据库,可以直接运行或者作为例子参考
