package org.beetl.sql.core.engine.template;

public interface TemplateContext {
    public Object getVar(String name);
}
