package org.beetl.sql.annotation.builder;
/**
 * 字段填充策略
 * @author whcrow
 * @see UpdateTime
 */
public enum FillStrategy {

	/**
	 * 插入时填充字段
	 */
	INSERT,

	/**
	 * 更新时填充字段
	 */
	UPDATE,

	/**
	 * 插入和更新时填充字段
	 */
	INSERT_UPDATE
}
